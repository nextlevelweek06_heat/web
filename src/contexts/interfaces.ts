import { ReactNode } from "react";

export interface User {
  id: string;
  avatar_url: string;
  name: string;
  login: string;
}

export interface AuthContextData {
  user: User | null;
  signInUrl: string;
  signOut: () => void;
  isLoading: boolean;
}

export interface AuthProviderProps {
  children: ReactNode;
}
