import { createContext, useEffect, useState } from "react";
import { AuthResponse } from "../components/LoginBox/interfaces";
import { api } from "../services/api";
import { AuthContextData, AuthProviderProps, User } from "./interfaces";

const STORAGE_TOKEN = "@dowhile:token";

export const AuthContext = createContext({} as AuthContextData);

export const AuthProvider = (props: AuthProviderProps) => {
  const [user, setUser] = useState<User | null>(null);
  const [isLoading, setIsLoading] = useState(false);

  const signInUrl = `https://github.com/login/oauth/authorize?scope=user&client_id=${
    import.meta.env.VITE_GITHUB_CLIENT_ID
  }`;

  const signIn = async (githubCode: string) => {
    setIsLoading(true);
    const response = await api.post<AuthResponse>("authenticate", {
      code: githubCode,
    });
    setIsLoading(false);

    const { token, user } = response.data;

    localStorage.setItem(STORAGE_TOKEN, token);

    api.defaults.headers.common.authorization = `Bearer ${token}`;

    setUser(user);
  };

  const signOut = () => {
    setUser(null);
    localStorage.removeItem(STORAGE_TOKEN);
  };

  useEffect(() => {
    const githubCodeQueryParam = "?code=";
    const url = window.location.href;
    const hasGithubCode = url.includes(githubCodeQueryParam);

    if (hasGithubCode) {
      const [urlWithoutGithubCode, githubCode] =
        url.split(githubCodeQueryParam);
      window.history.pushState({}, "", urlWithoutGithubCode);

      signIn(githubCode);
    }
  }, []);

  useEffect(() => {
    const token = localStorage.getItem(STORAGE_TOKEN);

    if (token) {
      api.defaults.headers.common.authorization = `Bearer ${token}`;

      setIsLoading(true);
      api.get<User>("profile").then((resp) => {
        setUser(resp.data);
      });
      setIsLoading(false);
    }
  }, []);

  return (
    <AuthContext.Provider value={{ signInUrl, user, signOut, isLoading }}>
      {props.children}
    </AuthContext.Provider>
  );
};
