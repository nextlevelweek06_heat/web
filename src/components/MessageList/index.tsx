import styles from "./styles.module.scss";
import { IResponseMessage } from "./interfaces";

import io from "socket.io-client";

import { api } from "../../services/api";
import logoImg from "../../assets/logo.svg";
import { useEffect, useState } from "react";

const messagesQueue: IResponseMessage[] = [];

const socket = io("http://localhost:4000");

socket.on("new_message", (newMessage: IResponseMessage) => {
  messagesQueue.push(newMessage);
});

export const MessageList = () => {
  const [messages, setMessages] = useState<IResponseMessage[]>([]);

  useEffect(() => {
    const timer = setInterval(() => {
      if (messagesQueue.length > 0) {
        setMessages((prevMessages) =>
          [messagesQueue[0], prevMessages[0], prevMessages[1]].filter(
            (m) => !!m
          )
        );

        messagesQueue.shift();
      }
    }, 3000);

    return () => clearInterval(timer);
  });

  useEffect(() => {
    api
      .get<IResponseMessage[]>("messages/last3")
      .then(({ data }: { data: IResponseMessage[] }) => {
        console.log(data);
        setMessages(data);
      });
  }, []);

  const messageComponent = (message: IResponseMessage) => (
    <li key={message.id} className={styles.message}>
      <p className={styles.messageContent}>{message.text}</p>
      <div className={styles.messageUser}>
        <div className={styles.userImage}>
          <img src={message.user.avatar_url} alt={message.user.name} />
        </div>
        <span>{message.user.name}</span>
      </div>
    </li>
  );

  return (
    <div className={styles.messageListWrapper}>
      <img src={logoImg} alt="DoWhile2021" />

      <ul className={styles.messageList}>
        {messages.map((message) => messageComponent(message))}
      </ul>
    </div>
  );
};
