import { User } from "../../contexts/interfaces";

export interface AuthResponse {
  token: string;
  user: User;
}
