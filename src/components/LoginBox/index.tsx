import { useContext } from "react";
import { VscGithubInverted } from "react-icons/vsc";
import { AuthContext } from "../../contexts/auth";

import styles from "./styles.module.scss";

export const LoginBox = () => {
  const { signInUrl, isLoading } = useContext(AuthContext);

  return (
    <div className={styles.loginBoxWrapper}>
      {isLoading ? (
        <strong className={styles.loading}>Carregando...</strong>
      ) : (
        <>
          <strong>Entre e compartilhe sua mensagem</strong>
          <a href={signInUrl} className={styles.signInWithGithub}>
            <VscGithubInverted size={24} />
            Entrar com Github
          </a>
        </>
      )}
    </div>
  );
};
